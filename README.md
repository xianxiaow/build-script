# build-script

基于webpack的前端项目构建工具。

只支持less, typescript。

```bash
npm install -g @rocksandy/build-script

build-script -h

Usage: build-script [options]

Options:

  -V, --version       output the version number
  --entry <value>     webpack entry文件, 默认src/main.tsx
  --no-cache          清除node_modules目录
  -m, --mode <value>  构建模式 prod | dev | local, 默认local
  -f, --fsize <n>     将小于size的资源文件进行base64编码合并,单位byte,默认0
  -w, --watch         启用watch模式
  -s, --sourcemap     生成sourcemap文件
  -h, --help          output usage information
```

# package.json

源代码项目的 package.json 需要添加以下字段：

```json
buildPublicPath: {
  "prod": "生产服务器地址",
  "dev": "测试服务器地址",
  "local": "本地调式地址"
}
```
