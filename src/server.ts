import * as webpack from 'webpack';
import * as middleware from 'webpack-dev-middleware';
import * as express from 'express';
import * as history from 'connect-history-api-fallback';
import webpackConfig from './webpack.config';

export default (args, config) => {
  const app = express();

  const compiler = webpack(webpackConfig(args, config));
  app.use(history());
  app.use(middleware(compiler, {
    logLevel: 'error',
    watchOptions: {
      aggregateTimeout: 500,
      ignored: /node_modules/,
      poll: 1000
    }
  }));

  app.listen(args.port, () => console.log('dev-server listening on port '+ args.port))
}
