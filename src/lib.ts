import * as path from 'path';
import chalk from 'chalk';
import * as webpack from 'webpack';
import * as shelljs from 'shelljs';
import { isFsExists } from './util';
import webpackConfig from './webpack.config';


export default (args, config) => {
  // 目标项目的node_modules目录
  const pathNodeModules = path.resolve(args.projectPath, 'node_modules');

  // 清空node_modules
  if (!args.cache) {
    shelljs.rm('-rf', pathNodeModules);
  }
  // 为了加速构建过程，不存在node_modules目录时，才进行npm install
  if (!isFsExists(pathNodeModules)) {
    shelljs.exec('npm install');
  }
  shelljs.cd(args.scriptPath);

  console.log(chalk.cyan(`building for ${args.buildType}... \n`));
  shelljs.rm('-rf', config.outDir); // 清除上次构建的文件

  const compiler = webpack(webpackConfig(args, config));

  compiler.run((err, stats) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    if (stats.hasErrors()) {
      console.error(stats.toJson('minimal'));
      process.exit(1);
    }
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n');

    console.log(chalk.cyan('  Build complete.\n'))
  });

}


