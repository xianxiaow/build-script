/**
 * 处理webpack的配置
 */
import {resolve, posix} from 'path';

export const assetsPath = (p: string) => {
  return posix.join('./static', p);
}

export default (args) => {
  const pkg = require(resolve(args.projectPath, 'package.json'));
  const outDir = resolve(args.projectPath, 'dist');

  const publicPath = pkg.buildPublicPath[args.buildType];
  const runtimeEnv = {
    NODE_ENV: args.buildType === 'local'? '"development"' : '"production"',
    PUBLIC_PATH: `"${publicPath}"`,
    BUILD_TYPE: `"${args.buildType}"`
  };

  const outFileJs =
    args.buildType === 'local' ?
    assetsPath('js/[name].js') :
    assetsPath('js/[name].[chunkhash].js');

  const outFileJsChunk =
    args.buildType === 'local' ?
    assetsPath('js/[id].js') :
    assetsPath('js/[id].[chunkhash].js');

  const outFileCss =
    args.buildType === 'local'?
    assetsPath('css/[name].css'):
    assetsPath('css/[name].[chunkhash].css');

  const outFileCssChunk =
    args.buildType === 'local'?
    assetsPath('css/[id].css'):
    assetsPath('css/[id].[chunkhash].css');

  return {
    assetsPath,
    /**
     * webpack 输出目录
     */
    outDir,
    /**
     * 路径前缀
     */
    publicPath,
    /**
     * 代码运行时的环境变量
     */
    runtimeEnv,
    /**
     * webpack 输出的JS文件名称
     */
    outFileJs,
    /**
     * webpack 输出的公共模块文件名称
     */
    outFileJsChunk,
    /**
     * webpack 输出的css文件
     */
    outFileCss,
    /**
     * webpack 输出的公共css文件
     */
    outFileCssChunk,
    /**
     * 输入的Html模板
     */
    inFileHtml: resolve(args.projectPath, 'src/index.html'),
    /**
     * 输出的Html文件地址
     */
    outFileHtml: resolve(args.projectPath, 'dist/index.html'),
    /**
     * 需要copy的源资源目录
     */
    inCopyDir: resolve(args.projectPath, 'static'),
    /**
     * 资源文件copy的目标目录
     */
    outCopyDir: resolve(args.projectPath, 'dist/static')
  }
}
