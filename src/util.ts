import * as fs from 'fs';

export const isFsExists = (p: string): boolean => {
  try{
    fs.accessSync(p, fs.constants.F_OK);
  }catch(e){
    return false;
  }
  return true;
}
