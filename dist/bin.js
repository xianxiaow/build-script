#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const program = require("commander");
const lib_1 = require("./lib");
const server_1 = require("./server");
const config_1 = require("./config");
program
    .version(require('../package.json').version)
    .option('--entry <value>', 'webpack entry文件, 默认src/main.tsx')
    .option('--no-cache', '清除node_modules目录')
    .option('-m, --mode <value>', '构建模式 prod | dev | local, 默认local')
    .option('-f, --fsize <n>', '将小于size的资源文件进行base64编码合并,单位byte,默认0', parseInt)
    .option('-s, --sourcemap', '生成sourcemap文件')
    .option('-a, --analyzer', '生产analyzer报告')
    .option('-p, --port <n>', '启动server并指定端口', parseInt);
program.parse(process.argv);
const projectPath = process.cwd();
const args = {
    scriptPath: path.resolve(__dirname, '../'),
    projectPath,
    /**
     * 目标项目入口文件
     */
    entryFile: path.resolve(projectPath, program.entry || './src/main.tsx'),
    /**
     * 是否清除node_modules, 默认否
     */
    cache: program.cache,
    /**
     * 构建类型 prod || env || local，默认local
     */
    buildType: program.mode || 'local',
    /**
     * 将小于size的资源文件进行base64编码合并
     */
    fsize: program.fsize || 1,
    /**
     * webpack devtool 参数
     */
    devtools: program.sourcemap ? '#source-map' : null,
    /**
     * 生产analyzer报告
     */
    analyzer: !!program.analyzer,
    /**
     * 启动dev-server, 并指定端口
     */
    port: program.port || 0
};
const cfg = config_1.default(args);
if (args.port) {
    server_1.default(args, cfg);
}
else {
    lib_1.default(args, cfg);
}
