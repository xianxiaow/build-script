"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const webpack = require("webpack");
const middleware = require("webpack-dev-middleware");
const express = require("express");
const history = require("connect-history-api-fallback");
const webpack_config_1 = require("./webpack.config");
exports.default = (args, config) => {
    const app = express();
    const compiler = webpack(webpack_config_1.default(args, config));
    app.use(history());
    app.use(middleware(compiler, {
        logLevel: 'error',
        watchOptions: {
            aggregateTimeout: 500,
            ignored: /node_modules/,
            poll: 1000
        }
    }));
    app.listen(args.port, () => console.log('dev-server listening on port ' + args.port));
};
