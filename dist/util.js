"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
exports.isFsExists = (p) => {
    try {
        fs.accessSync(p, fs.constants.F_OK);
    }
    catch (e) {
        return false;
    }
    return true;
};
