"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const tsconfig_paths_webpack_plugin_1 = require("tsconfig-paths-webpack-plugin");
const WebpackBar = require("webpackbar");
const Analyzer = require("webpack-bundle-analyzer");
exports.default = (args, config) => {
    const rules = [
        { test: /\.(tsx|ts)$/, loader: 'ts-loader' },
        {
            test: /\.less$/,
            use: [
                MiniCssExtractPlugin.loader,
                { loader: "css-loader" },
                {
                    loader: "postcss-loader",
                    options: {
                        ident: 'postcss',
                        plugins: (loader) => [require('autoprefixer')]
                    }
                },
                { loader: "less-loader" }
            ]
        }, {
            test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
            loader: 'url-loader',
            options: {
                limit: args.fsize,
                name: config.assetsPath('img/[name].[hash:7].[ext]'),
            }
        }, {
            test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
            loader: 'url-loader',
            options: {
                limit: args.fsize,
                name: config.assetsPath('fonts/[name].[hash:7].[ext]')
            }
        }
    ];
    const optimization = {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                },
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendors",
                    chunks: "all"
                }
            }
        },
        runtimeChunk: {
            name: (entrypoint) => `runtime.${entrypoint.name}`
        }
    };
    const plugins = [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': config.runtimeEnv.NODE_ENV,
            'process.env.PUBLIC_PATH': config.runtimeEnv.PUBLIC_PATH,
            'process.env.BUILD_TYPE': config.runtimeEnv.BUILD_TYPE
        }),
        new CopyWebpackPlugin([{
                from: config.inCopyDir,
                to: config.outCopyDir,
                ignore: ['.*']
            }]),
        new UglifyJsPlugin({
            uglifyOptions: {
                beautify: false,
                ecma: 8,
                compress: true,
                comments: false
            }
        }),
        new OptimizeCSSAssetsPlugin({}),
        new HtmlWebpackPlugin({
            filename: config.outFileHtml,
            template: config.inFileHtml,
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true
            },
            chunksSortMode: 'dependency'
        }),
        new MiniCssExtractPlugin({
            filename: config.outFileCss,
            chunkFilename: config.outFileCssChunk
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new WebpackBar(),
    ];
    if (args.analyzer) {
        plugins.push(new Analyzer.BundleAnalyzerPlugin());
    }
    return {
        entry: {
            app: args.entryFile
        },
        output: {
            path: config.outDir,
            filename: config.outFileJs,
            chunkFilename: config.outFileJsChunk,
            publicPath: config.publicPath
        },
        resolve: {
            extensions: ['.js', '.ts', '.tsx', '.json'],
            plugins: [
                new tsconfig_paths_webpack_plugin_1.default({
                    configFile: path.resolve(args.projectPath, 'tsconfig.json')
                })
            ]
        },
        module: { rules },
        optimization,
        plugins,
        devtool: args.devtool,
        mode: args.buildType === 'local' ? 'development' : 'production'
    };
};
