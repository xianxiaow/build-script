"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * 处理webpack的配置
 */
const path_1 = require("path");
exports.assetsPath = (p) => {
    return path_1.posix.join('./static', p);
};
exports.default = (args) => {
    const pkg = require(path_1.resolve(args.projectPath, 'package.json'));
    const outDir = path_1.resolve(args.projectPath, 'dist');
    const publicPath = pkg.buildPublicPath[args.buildType];
    const runtimeEnv = {
        NODE_ENV: args.buildType === 'local' ? '"development"' : '"production"',
        PUBLIC_PATH: `"${publicPath}"`,
        BUILD_TYPE: `"${args.buildType}"`
    };
    const outFileJs = args.buildType === 'local' ?
        exports.assetsPath('js/[name].js') :
        exports.assetsPath('js/[name].[chunkhash].js');
    const outFileJsChunk = args.buildType === 'local' ?
        exports.assetsPath('js/[id].js') :
        exports.assetsPath('js/[id].[chunkhash].js');
    const outFileCss = args.buildType === 'local' ?
        exports.assetsPath('css/[name].css') :
        exports.assetsPath('css/[name].[chunkhash].css');
    const outFileCssChunk = args.buildType === 'local' ?
        exports.assetsPath('css/[id].css') :
        exports.assetsPath('css/[id].[chunkhash].css');
    return {
        assetsPath: exports.assetsPath,
        /**
         * webpack 输出目录
         */
        outDir,
        /**
         * 路径前缀
         */
        publicPath,
        /**
         * 代码运行时的环境变量
         */
        runtimeEnv,
        /**
         * webpack 输出的JS文件名称
         */
        outFileJs,
        /**
         * webpack 输出的公共模块文件名称
         */
        outFileJsChunk,
        /**
         * webpack 输出的css文件
         */
        outFileCss,
        /**
         * webpack 输出的公共css文件
         */
        outFileCssChunk,
        /**
         * 输入的Html模板
         */
        inFileHtml: path_1.resolve(args.projectPath, 'src/index.html'),
        /**
         * 输出的Html文件地址
         */
        outFileHtml: path_1.resolve(args.projectPath, 'dist/index.html'),
        /**
         * 需要copy的源资源目录
         */
        inCopyDir: path_1.resolve(args.projectPath, 'static'),
        /**
         * 资源文件copy的目标目录
         */
        outCopyDir: path_1.resolve(args.projectPath, 'dist/static')
    };
};
